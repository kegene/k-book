# Terminal

## 資源

- [字典](https://ss64.com/)
- [簡單易懂的 cmd 教學](https://carolhsu.gitbooks.io/django-girls-tutorial-traditional-chiness/content/intro_to_command_line/README.html)

## 好用的指令

### 「還我乾淨的畫面」`clear` 清理日誌

```
clear
```

### 「搜尋」`grep` 搜尋關鍵字

```
grep hello file?.type
```

## 移動

### 「我在哪？」`pwd` 目前位置

當迷失方向的時候可以用。

```
pwd
```

### 「去 xxx。」`cd` 移動

到使用者資料夾: `cd ~`

到根目錄: `cd /`

到上一層: `cd ..`

### 「關閉視窗」`exit` 離開

```
exit
```

## 觀看

### 「查看文件內容。」`cat` 看文件內容

```
cat file.type
```

### 「資料夾有什麼東西？」`ls` 當前位置有什麼東西

簡易的展示: `ls`

詳細的展示: `ls -s` 或者 `ll`

詳細的展示，包含隱藏檔案: `ls -al`

::: tip 提示
Window: `dir`

OS: `ls`
:::

## 操作檔案

### 「新增資料夾。」`mkdir` 創建資料夾

```
mkdir <dir name>
```

### 「刪除資料夾。」`rmdir` 刪除空的資料夾

```
rmdir <dir name>
```

參數 `-p`: 刪除包含在內的空資料夾

:::warning 注意
刪除資料夾用`rmdir`，也可以使用`rm`加上參數進行資料夾刪除，詳情請參閱`rm`說明。
:::

### 「創建檔案」`touch` 更新或創建檔案

```
touch file.type
```

::: tip 提示
如果檔案已存在，則更新檔案時間。
如果檔案不存在，則創建檔案。
:::

### 「移動某個檔案、重新命名」`mv` 變更檔案 位置或名字

操作：file.type 改名為 file2.type

```
mv file.type file2.type
```

操作：file2.type 放進 dir2 資料夾

```
mv file2.type ./dir2
```

::: tip 提示
Window: `move`

OS: `mv`
:::

### 「刪除東西」`rm` 刪除檔案或目錄

試試看刪除檔案，像這樣

```
rm file1.type
```

再試試看使用 rm 刪除資料夾，像這樣

```
rm dir2
```

然後跟我一樣會得到
:::danger Error
無法刪除，這是目錄。
:::
~~ㄏㄏ~~

。。。

網路上看到大家都這樣刪除資料：

```
rm -rf dir2
```

沒錯就是這樣，請照做謝謝

。。。

參數 `-r`: 遞回執行，表示包含在內的檔案都執行刪除，斷開鎖鏈，斷開魂結，通通銷毀。

參數 `-f`: 不給提示，就像開靜音模式。

其他不常用，請查`rm --help`

::: tip 提示
Window: `del`

OS: `rm`
:::

### 「我要複製」`cp` 複製

複製檔案

```
cp file.type newFile.type
```

複製資料夾

```
cp -r dir1 dir2
```

參數 `-r`：遞回執行，複製好複製滿

::: tip 提示

Window: `copy`

OS: `cp`
:::

### 「匯出內容」`>` 輸出到某個地方

像這樣最後 a.txt 內容會是 hellooooo

```
echo hello > a.txt
grep hello file.type > a.txt
grep hellooooo file.type > a.txt
```

1. 打印 hello 存進 a.txt
2. 搜尋 file.type 內的 hello，搜尋到的結果，存進 a.txt
3. 搜尋 file.type 內的 hellooooo，搜尋到的結果，存進 a.txt

---

### 實戰練習

此時的資料夾結構：

```
.
```

#### 1.創建資料夾，輸入：

```
mkdir dir1
mkdir dir2
```

此時的資料夾結構：

```
.
├─ dir1
└─ dir2
```

#### 2.創建檔案，輸入：

```
touch file1.type
touch file2.type
```

此時的資料夾結構：

```
.
├─ dir1
├─ dir2
├─ file1.type
└─ file2.type
```

#### 3.刪除資料夾、刪除檔案，輸入：

```
rmdir dir1
rm file1.type
```

此時的資料夾結構：

```
.
├─ dir2
└─ file2.type
```

#### 4.更新檔名，輸入：

```
mv file2.type file1.type
```

此時的資料夾結構：

```
.
├─ dir2
└─ file1.type
```

#### 5.移動檔案，輸入：

```
mv file1.type ./dir2
```

此時的資料夾結構：

```
.
└─ dir2
   └─ file1.type
```

#### 6.複製一份，輸入：

```
cp -r dir2 dir1
```

此時的資料夾結構：

```
.
├─ dir1
│ └─ file1.type
└─ dir2
└─ file1.type
```

#### 7.最後，輸入：

```
echo bye bye! > end.txt
clear
```
