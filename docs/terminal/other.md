# 網路連線

## nslookup

找出域名的 ip 位置

```
nslookup google.com
```

## traceroute

查看從當前 ip 抵達 對象 ip，經過多少封包

```
traceroute google.com
```

## telnet

連線到哪個 ip，像平常訪問頁面一樣。

```
telnet google.com
```
