# vue

## 基本知識

## vue-cli

### 執行 `vue ui` 創建 csr-template

## Vuex

## SSR

### Nuxt.js

### 第一種初始

1. 建立 package.json

```
{
  "name": "my-app",
  "scripts": {
    "dev": "nuxt",
    "build": "nuxt build",
    "generate": "nuxt generate",
    "start": "nuxt start"
  },
  "dependencies": {
    "nuxt": "^2.14.3"
  }
}
```

2. `yarn` or `npm i`
3. 完成 ssr-template1

### 第二種初始

1. 執行 `create nuxt-app`
2. [create 過程中會問的問題](https://github.com/nuxt/create-nuxt-app)
3. 完成 ssr-template2
