# Node隨手記

===

[TOC]

## global，Node全域屬性，就像網頁的window

每一個js檔案的global都是獨立分開的。就像一個頁面有一個window。

## require、module export

當要引入js，可以這樣用

testA.js

```javascript

let demo2 = require('./les2');
console.log("require('./les2'):",demo2); // 2
demo2.barkDemoA(); // hello

```

les2.js

```javascript

var les2 = 2;

// 方法一 使用字面表示法
// 方法一會覆蓋方法二;
// module.exports = les2; // 返回基本數 2
module.exports = {
  les2DemoA: les2 ,
  barkDemoA: function() { console.log('hello') }
};

// 方法二
exports.les2DemoB = les2;
exports.barkDemoB = function() { console.log('hello') };

```
