const nav = require('./router/nav.js');
const sidebar = require('./router/sidebar.js');

module.exports = {
  dest: 'public',
  base: '/k-book/',
  themeConfig: {
    nav,
    sidebar,
    lastUpdated: 'Last Updated', // string | boolean
  },
  head: [
      ['script', {src: 'https://code.iconify.design/1/1.0.7/iconify.min.js'}]
  ],
  title: '大雜燴',
  description: '紀錄學習歷程及各種瑣事',
  markdown: {
    // lineNumbers: true,
    // markdown-it-anchor 的选项
    // anchor: { permalink: false },
    // markdown-it-toc 的选项
    // toc: { includeLevel: [1, 2] },
    // extendMarkdown: md => {
    // 使用更多的 markdown-it 插件!
    // md.use(require('markdown-it-xxx'))
    // }
  },
  // configureWebpack: {
  //   resolve: {
  //     alias: {
  //       '@docs': '/docs',
  //     },
  //   },
  // },
  plugins: [
    // GA
    [
      '@vuepress/google-analytics',
      {
        ga: 'UA-175436033-1', // UA-00000000-0
      },
    ],
    // 圖片放大
    [
      '@vuepress/medium-zoom',
      {
        selector: 'img.zoom-custom-imgs',
        // medium-zoom options here
        // See: https://github.com/francoischalifour/medium-zoom#options
        options: {
          margin: 16,
        },
      },
    ],
  ],
};
