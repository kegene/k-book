module.exports = [
  {
    text: '筆記',
    ariaLabel: '筆記',
    items: [{ text: 'computer', link: '/note/computer' }],
  },
  {
    text: '更多',
    ariaLabel: '更多',
    items: [
      {
        text: 'terminal',
        items: [
          { text: '基本指令', link: '/terminal/main' },
          { text: '其他', link: '/terminal/other' },
        ],
      },
      {
        text: 'other',
        items: [
          { text: 'ssh', link: '/ssh/main' },
          { text: 'git', link: '/git/main' },
        ],
      },
    ],
  },
  {
    text: 'life',
    ariaLabel: 'life',
    items: [{ text: 'life', link: '/life/' }],
  },
];
