// module.exports = {
//   '/bar/': [
//     '' /* /foo/ */,
//     'three' /* /foo/one.html */,
//     'four' /* /foo/two.html */,
//   ],
//   '/guide/': ['' /* /foo/ */, 'test' /* /foo/one.html */],
// };

// sidebar: 'auto',
// sidebar: ['/', '/bar/three', ['/bar/four', 'Explicit link text']],
// module.exports = [
//   {
//     title: 'Group 1', // 必要的
//     path: '/bar', // 可选的, 标题的跳转链接，应为绝对路径且必须存在
//     collapsable: false, // 可选的, 默认值是 true,
//     sidebarDepth: 1, // 可选的, 默认值是 1
//     children: ['/bar/three', '/bar/four'],
//   },
// ],
module.exports = 'auto';
