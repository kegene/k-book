---
home: true
heroText: 筆記大雜燴
tagline: 一些自言自語紀錄
actionText: 打開筆記
actionLink: /note/
features:
  - title: TODO 最新文章
    details: 最新內容

footer: MIT Licensed | Copyright © 2018-present Evan You

tags:
  - 配置
  - 主题
  - 索引
meta:
  - name: description
    content: hello
  - name: keywords
    content: super duper SEO
---

## 首頁好 copy

[表情符號](https://github.com/markdown-it/markdown-it-emoji/blob/master/lib/data/full.json)

::: tip 提示
这是一个提示
:::

```
::: tip 提示
这是一个提示
:::
```

::: warning
这是一个警告
:::

```
::: warning
这是一个警告
:::
```

::: danger
这是一个危险警告
:::

```
::: danger
这是一个危险警告
:::
```

::: details
这是一个详情块，在 IE / Edge 中不生效
:::

```
::: details
这是一个详情块，在 IE / Edge 中不生效
:::
```

::: danger STOP
危险区域，禁止通行
:::

```
::: danger STOP
危险区域，禁止通行
:::
```

::: details 点击查看代码
777
:::

```
::: details 点击查看代码
777
:::
```

---

```js
console.log('你好，VuePress！');
```

````
```js
console.log('你好，VuePress！');
```
````

---

```js
export default {
  name: 'MyComponent',
  // ...
};
```

````
```js
export default {
  name: 'MyComponent',
  // ...
};
```
````

---

```html
<ul>
  <li v-for="todo in todos" :key="todo.id">
    {{ todo.text }}
  </li>
</ul>
```

````
```html
<ul>
  <li v-for="todo in todos" :key="todo.id">
    {{ todo.text }}
  </li>
</ul>
```
````

---

```js {4}
export default {
  data() {
    return {
      msg: 'Highlighted!',
    };
  },
};
```

````
```js {4}
export default {
  data() {
    return {
      msg: 'Highlighted!',
    };
  },
};
```
````

---

```
.
└─ .vuepress
   └─ components
      ├─ demo-1.vue
      ├─ OtherComponent.vue
      └─ Foo
         └─ Bar.vue
```

tip 靠下<Badge text="文字" type="tip" vertical="bottom" />

green 靠上<Badge text="文字" type="green" vertical="top" />

warning 靠中<Badge text="文字" type="warning" vertical="top" />

warn 靠上<Badge text="文字" type="warn" vertical="top" />

yellow 靠上<Badge text="文字" type="yellow" vertical="top" />

error 靠上<Badge text="文字" type="error" vertical="top" />

```html
tip 靠下<Badge text="文字" type="tip" vertical="bottom" />

green 靠上<Badge text="文字" type="green" vertical="top" />

warning 靠中<Badge text="文字" type="warning" vertical="top" />

warn 靠上<Badge text="文字" type="warn" vertical="top" />

yellow 靠上<Badge text="文字" type="yellow" vertical="top" />

error 靠上<Badge text="文字" type="error" vertical="top" />
```

- docs/.vuepress: 用于存放全局的配置、组件、静态资源等。
- docs/.vuepress/components: 该目录中的 Vue 组件将会被自动注册为全局组件。
- docs/.vuepress/theme: 用于存放本地主题。
- docs/.vuepress/styles: 用于存放样式相关的文件。
- docs/.vuepress/styles/index.styl: 将会被自动应用的全局样式文件，会生成在最终的 CSS 文件结尾，具有比默认样式更高的优先级。
- docs/.vuepress/styles/palette.styl: 用于重写默认颜色常量，或者设置新的 stylus 颜色常量。
- docs/.vuepress/public: 静态资源目录。
- docs/.vuepress/templates: 存储 HTML 模板文件。
- docs/.vuepress/templates/dev.html: 用于开发环境的 HTML 模板文件。
- docs/.vuepress/templates/ssr.html: 构建时基于 Vue SSR 的 HTML 模板文件。
- docs/.vuepress/config.js: 配置文件的入口文件，也可以是 YML 或 toml。
- docs/.vuepress/enhanceApp.js: 客户端应用的增强。
