# Git(版控)

版本控制

## 資源

- [docs.github](https://docs.github.com/cn/github)

## 開始使用 git

### mac 安裝 git

1. [download for macOS](https://git-scm.com/download/mac)

使用[Homebrew](https://brew.sh/)安裝 git

```
brew install git
```

2. 檢查 git 是否安裝完成

```
git --version
```

3. 設置 git 用戶名、信箱

`--global`：全局。

```
git config --global user.name [name]
git config --global user.email [email]
```

檢查剛剛設置的全局 git 用戶名、信箱

```
git config --global user.name
git config --global user.email
```

::: tip 檢查全部設置

```
git config --list
```

:::

### 如何使用 git

直接看推薦文章書籍：[寫點科普](https://kopu.chat/2017/01/18/git%e6%96%b0%e6%89%8b%e5%85%a5%e9%96%80%e6%95%99%e5%ad%b8-part-1/)或是看[為你自己學 git](https://gitbook.tw/)。

以下是簡要指令版本

#### 1. 開始使用 git

在專案底下：

```
git init
```

#### 2. 檢查當前檔案狀態

```
git status
```

你會看到哪些檔案是新建、修改、刪除、哪些檔案已經放進暫存區。

#### 3. 把想要存檔的檔案加入暫存區

```
git add [file.type]
```

`add`: 加入暫存區。

#### 4. 替暫存區 備註訊息，訊息內容主要說明本次更改內容。

```
git commit -m '新增一筆紀錄'
```

`commit`: 替當前暫存區 備註訊息，並生成 SHA 綁定此筆訊息。

`-m`: 要輸入訊息。

**enter 成功後，會把這次資料保存為一個版本(Repository)，像是打遊戲的存檔紀錄，隨時回去。**

### 線上保存 Repository

#### 1. 選一個平台註冊

平台可以提供遠端倉庫地址(remote)，用來保存檔案、維護專案檔案、操作 CI/CD 自動處理些事情、霹哩啪拉...。

大眾所選：[github](https://github.com/)、[gitlab](https://about.gitlab.com/)

#### 2. 替專案新增遠端倉庫位置

```
git remote add [new remote name] [remote 地址]
```

比如說

```
git remote add origin git@gitxxxx.git
```

`remote add`: remote 名單加入新的 remote。

通常 remote name 會取名 origin，表示主遠端地址。

#### 3. 把此次版本推送到遠端倉庫保存起來。

```
git push [remote name] [branch]
```

比如說

```
git push origin git@gitxxxx.git
```

成功後，查看平台上就會出現剛剛推上去的最新紀錄。

::: tip 延伸閱讀

- 給平台設定 ssh，使用 ssh 取代登入操作，省去輸入 平台的帳號密碼。

[SSH](/docs/ssh/main.md)、[docs.github 的 ssh 教學](https://docs.github.com/cn/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#further-reading)。

- 創建白名單(.gitignore)，白名單內的檔案不需版控。[範本](https://gist.github.com/octocat/9257657)

```
touch .gitignore
```

:::

::: warning 提交者與登入者？

本機設置的 git 用戶名與郵箱＝提交者。

版控平台帳號＝登入者。

假設今天使用 github 平台，登入 cat 帳戶，而本機設置的 git 用戶設置為 dog，那推送上去會顯示 dog 名字。

:::

## 設置 config

### 全局設置加上參數--global

`git config --global [xxx]`

### 設置 git 用戶名

`git config user.name [name]`

### 設置 git 郵箱

`git config user.name [email]`

### CRLF 及 LF 紀錄

預設是 ture

`git config --global core.autocrlf [boolean]`

[延伸閱讀](https://docs.github.com/cn/github/using-git/configuring-git-to-handle-line-endings)

## 查字典

### remote 遠端地址

remote 幫助中心：`git remote -help`

#### 新增 遠端地址

`git remote add [remote name] [href]`

比如新增一個 remote 取名叫做 origin

```
git remote add origin new.git.url/here
```

#### 查看 遠端地址

```
git remote
```

#### 修改 遠端地址

`git remote set-url [remote name] [href]`

比如修改 origin 地址為 new.git

```
git remote set-url origin new.git.url/here
```

#### 刪除 遠端地址

`git remote remove [remote name]`

比如替本專案刪除遠端地址 "origin"

```
git remote remove origin
```

### branch 分支

branch 幫助中心：`git branch -help`

#### 檢視所有本地分支

```
git branch
```

#### 檢視所有遠端分支

```
git branch -r
```

#### 檢視本地和遠端所有的分支

```
git branch -a
```

#### 刪除本地分支(強制)

不論你這支分支有無與主分支合併。

```
git branch -D branch
```

#### 刪除本地分支(有提示)

若分支未合併到主分支，則不給刪除

```
git branch -d branch
```

#### 刪除遠端分支

```
git branch -d -r origin/branchA
```

#### 刪除遠端已不存在的分支 branch

```
git fetch --prune
或者簡略：
git fetch -p
git fetch -p origin
git remote prune origin
```

### 拉(獲取)遠端分支 Repository

`git pull [remote name] [branch name]`

給本地來一份遠端 beta 分支 Repository，通常會直譯說：拉下來

> 比如會這樣對話
>
> 同事：你可以拉 beta 分支了。

```
git pull origin beta
```

### 推(發送)本地分支 Repository 到遠端

`git push [remote name] [branch name]`

把本地資料保存一份到遠端 beta 分支，通常也是直譯說：推上去。

> 比如會這樣對話
>
> 同事：我剛剛推(發)了一個版本到 beta，你拉(更新)一下吧！

```
git push origin beta
```

### checkout 切換分支

checkout 幫助中心：`git checkout -help`

##### 切換分支

```
git checkout [分支名]
```

##### 創建分支並切換至該分支

```
git checkout -b [分支名]
```

### 其他較常用的命令

#### 同步遠端紀錄

```
git fetch
```

### 回滾版本（返回紀錄的方法）

#### 教學看這裡

- [懶人 url | 為你自己學 git](https://gitbook.tw/chapters/using-git/reset-commit.html)

#### reset

commit 紀錄返回上一筆

```
git reset HEAD
```

如果要 reset 遠端分支

```
git reset -f origin/遠端分支名
```

::: danger 注意
`-f`: 強制覆蓋

如果是多人專案，請小心使用，他會把 commit 完全覆蓋成你推上去的版本紀錄。
:::

#### 遠端版本回滾 revert

```
git revert HEAD --no-edit
```

> 加上--no-edit 參數表示不編輯 commit 訊息
